from django.db import models
from django.core.validators import validate_email


class Food(models.Model):
	name=models.CharField(max_length=200)
	image=models.ImageField(upload_to="foods")
	description=models.TextField()
	price=models.DecimalField(max_digits=19,decimal_places=2)

	def __str__(self):
		return self.name

class Slider(models.Model):
	title=models.CharField(max_length=200)
	image=models.ImageField(upload_to="Slider")

	def __str__(self):
	  return self.title

class Testimonial(models.Model):
    name=models.CharField(max_length=200)
    image=models.ImageField(upload_to="Testimonial")
    organization=models.CharField(max_length=200)
    review=models.TextField()

    def __str__(self):
    	return self.name

class Gallery(models.Model):
    image=models.ImageField(upload_to="galleries")
    caption=models.CharField(max_length=500)

    def __str__(self):
       return self.caption 


class Subscriber(models.Model):
	name=models.CharField(max_length=500)
	email_address=models.EmailField(unique="TRUE",validators=[validate_email])

	def __str__(self):
		return self.caption


























# Create your models here.
