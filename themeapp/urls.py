from django.urls import path
from .views import *


app_name = "themeapp"
urlpatterns = [
	path("", HomeView.as_view(),name="home"),
    path("about_us/",AboutView.as_view(),name="about"),
    path("contact_us/",ContactView.as_view(),name="contact"),
    path("gallery/",GalleryView.as_view(),name="gallery"),


    path('my-admin/', AdminHomeView.as_view(), name="adminhome"),
    path("my-admin/food/list/", AdminFoodListView.as_view(), name="adminfoodlist"),
    path("my-admin/food/create/", AdminFoodCreateView.as_view(), name="adminfoodcreate"),
    
    path("my-admin/food/<int:pk>/update/", AdminFoodUpdateView.as_view(), name="adminfoodupdate"),
    path("my-admin/food/<int:pk>/delete/", AdminFoodDeleteView.as_view(), name="adminfooddelete"),

    path("login/",LoginView.as_view(),name="login"),
    path("logout/",LogoutView.as_view(),name="logout"),
]