from django import forms
from .models import *


class FoodForm(forms.ModelForm):
	class Meta:
		model = Food
		fields = ['name', 'image', 'description', 'price']
		widgets = {
			'name': forms.TextInput(attrs={
				'class': 'form-control mb-3',
				'placeholder': 'Enter food name...'
				}),
			'image': forms.ClearableFileInput(attrs={
				'class': 'form-control mb-3'
				}),
			'description': forms.Textarea(attrs={
				'class': 'form-control mb-3',
				'placeholder': 'Enter description...'
				}),
			'price': forms.NumberInput(attrs={
				'class': 'form-control mb-3',
				'placeholder':'Enter Price of the food..'
				})
		}

class LoginForm(forms.Form):
    username =forms.CharField(widget=forms.TextInput())
    password=forms.CharField(widget=forms.PasswordInput())		